<div align="center">
    <img src="https://cdn.w7.cc/ued/logo/icon.png" alt="">
</div>

### 微擎开源微信公众号管理系统

感谢您选择微擎系统。

微擎是一款免费开源的微信公众号管理系统，基于目前流行的WEB2.0架构（php+mysql），支持在线升级和安装模块及模板，拥有良好的开发框架、成熟稳定的技术解决方案、活跃的第三方开发者及开发团队，依托微擎开放的生态系统，提供丰富的扩展功能。

### 运行环境
Nginx、PHP >=5.6、MySQL>=5.7
运行微擎系统必须保证环境版本满足上述要求，具体环境检测可以运行 _install.php_ 文件进行检测。

### 目录结构
请确保您将微擎程序文件放置在您的网站目录中，微擎项目目录结构如下：
```
    addons             微擎模块
    api                对接外部系统接口
    app                微站 （Mobile / App）
    attachment         附件目录
    framework          微擎框架
    payment            支付调用目录
    tester             测试用例
    upgrade            升级脚本
    web                后台管理
    api.php            微信api接口
    index.php          系统入口
    install.php        安装文件
    console.php        命令行执行文件
```

### 在线安装
#### 支持注册站点，支持在线更新
请到 [这里](http://s.w7.cc/static/install)

### 本地安装（开发用）
#### 注意：此方式不会注册站点且无法在线更新
master分支的所有代码clone完成后，浏览器内输入：您的域名/install.php 来执行安装
#### 本地安装模式下应用的安装、更新和卸载
应用的安装、更新和卸载，系统提供了命令行模式，具体如下：
> 安装： php console.php module:install module_name=应用标识

> 更新： php console.php module:upgrade module_name=应用标识

> 卸载： php console.php module:uninstall module_name=应用标识

### 后续
您可以通过查看我们的文件，来对系统进一步的了解和开发、开发模块。查看文档请 [点击这里](https://wiki.w7.com/document/35/370) ，我们会不断的更新文档内容。
如果在文档中有未尽事宜您可以通过给我们 [提交工单](https://task.w7.com/taskrelease?c=self&type_id=10&team_id=1) 来与我们取得联系。当您模块开发完毕可以通过我们的应用商城进行 [发布](https://dev.w7.cc/)。

再次感谢您对我们的支持，欢迎您对我们的程序提出意见，我们期待您的Pull Request。
