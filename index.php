<?php
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * $sn: pro/index.php : v 815cdc81ea88 : 2015/08/29 09:40:39 : RenChao $
 */
require __DIR__ . '/framework/bootstrap.inc.php';

$host = $_SERVER['HTTP_HOST'];
if (!empty($host)) {
	$bindhost = pdo_fetch("SELECT * FROM " . tablename('site_multi') . " WHERE bindhost = :bindhost", array(':bindhost' => $host));
	if (!empty($bindhost)) {
		header("Location: " . $_W['siteroot'] . 'app/index.php?i=' . $bindhost['uniacid'] . '&t=' . $bindhost['id']);
		exit;
	}
	
}
if (!empty($_W['setting']['app_redirect']) && safe_gpc_url($_W['setting']['app_redirect']) && !empty($_GPC['w7_app_redirect'])) {
	$response = ihttp_get($_W['setting']['app_redirect']);
	if ($response['code'] == 200 && !empty($response['content'])) {
		die($response['content']);
	}
}
if ($_W['os'] == 'mobile' && (!empty($_GPC['i']) || !empty($_SERVER['QUERY_STRING']))) {
	header('Location: ' . $_W['siteroot'] . 'app/index.php?' . $_SERVER['QUERY_STRING']);
} else {
	if (!empty($_SERVER['QUERY_STRING'])) {
		header('Location: ' . $_W['siteroot'] . 'web/index.php?' . $_SERVER['QUERY_STRING']);
	} else {
		header('Location: ' . $_W['siteroot'] . 'web/index.php?c=home&a=home&do=home');
	}
}
