<?php
/**
 * [WeEngine System] Copyright (c) 2014 WE7.CC
 */
defined('IN_IA') or exit('Access Denied');

isetcookie('__iscontroller', 0);
$_W['iscontroller'] = 0;
if (!empty($_GPC['getmenu'])) {
	$home_menu = system_star_menu();
	iajax(0, $home_menu);
}
template('home/home');
