<?php
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC.
 */
defined('IN_IA') or exit('Access Denied');

$account_api = WeAccount::createByUniacid();
$account_type = $account_api->menuFrame;
define('FRAME', $account_type);