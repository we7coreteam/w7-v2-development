<?php
/**
 * 企业微信.
 *
 * [WeEngine System] Copyright (c) 2014 W7.CC
 */
defined('IN_IA') or exit('Access Denied');

$dos = array('display');
$do = in_array($do, $dos) ? $do : 'display';

if ('display' == $do) {
	$redirect = home_url();
	if (empty($_W['account'])) {
		itoast('', $redirect);
	}
	if (!empty($_W['account']['endtime']) && $_W['account']['endtime'] != USER_ENDTIME_GROUP_EMPTY_TYPE && $_W['account']['endtime'] != USER_ENDTIME_GROUP_UNLIMIT_TYPE && $_W['account']['endtime'] < time() && !$_W['isadmin']) {
		itoast('平台账号已到服务期限，请联系管理员', $redirect, 'info');
	}
	if (!empty($_W['account']['type_sign']) && 'wxapp' == $_W['account']['type_sign']) {
		itoast('', $redirect);
	}
	//公告
	load()->model('welcome');
	$notices = welcome_notices_get();
	template('wechatwork/home');
}
